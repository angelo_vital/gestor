interface PageLink {
    active: boolean;
    label: string;
    url: string;
}

export default PageLink;
