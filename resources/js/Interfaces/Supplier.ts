interface Supplier {
    id: number;
    name: string;
    cpf_cnpj: string;
    email: string;
    mobile: string;
    address: string;
    number: string;
    district: string;
    complement: string;
}

export default Supplier;
