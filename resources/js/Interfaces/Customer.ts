interface Customer {
    id: number;
    cod: string;
    name: string;
    email: string;
    mobile: string;
    cpfcnpj: string;
    address: string;
    number: string;
    district: string;
}

export default Customer;
