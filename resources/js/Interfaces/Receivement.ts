interface Receivement {
    date_payment: string;
    payment_method_id: string;
    received: string;
    obs: string;
}

export default Receivement;
