interface Category {
    id: number;
    title: string;
    type: string;
}

export default Category;
