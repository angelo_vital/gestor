interface Product {
    id: number;
    reference: string;
    description: string;
    type: string;
    price: string;
}

export default Product;
